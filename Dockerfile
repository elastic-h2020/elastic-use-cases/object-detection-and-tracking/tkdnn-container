ARG BUILD_IMAGE=nvcr.io/nvidia/l4t-base:r32.4.3
ARG BASE_IMAGE=${BUILD_IMAGE}

FROM ${BUILD_IMAGE} as builder

RUN apt-get update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    build-essential cmake git ninja-build \
    libgtk-3-dev python3-dev python3-numpy \
    ca-certificates file \
    libeigen3-dev libyaml-cpp-dev libssl-dev \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# CMAKE
WORKDIR /usr/local/src
ARG CTAG=v3.18.4
RUN git clone --depth 1 --branch ${CTAG} https://github.com/Kitware/CMake.git \
    && mkdir cmake_build

WORKDIR /usr/local/src/cmake_build
RUN cmake \
    -G Ninja \
    /usr/local/src/CMake
RUN ninja -j$(nproc) \
    && ninja install -j$(nproc)

# OPENCV
# https://docs.opencv.org/master/d2/de6/tutorial_py_setup_in_ubuntu.html
WORKDIR /usr/local/src
ARG CVTAG=4.5.0
RUN git clone --depth 1 --branch ${CVTAG} https://github.com/opencv/opencv.git \
    && git clone --depth 1 --branch ${CVTAG} https://github.com/opencv/opencv_contrib.git \
    && mkdir opencv_build

# Required opencv compiling dependencies
RUN echo "Installing build dependencies." && \
    apt update && \
    apt dist-upgrade -y --autoremove && \
    apt install -y \
	build-essential \
	unzip \
	pkg-config \
	libjpeg-dev \
	libpng-dev \
	libtiff-dev \
	libavcodec-dev \
	libavformat-dev \
	libswscale-dev \
	libv4l-dev \
	libxvidcore-dev \
	libx264-dev \	
	libgtk-3-dev \
	libatlas-base-dev \
	gfortran \
	python3-dev \
   libgstreamer1.0-dev \
   libgstreamer-plugins-base1.0-dev \
   libdc1394-22-dev \
   libavresample-dev \
   libgstreamer1.0-dev \
   libgstreamer-plugins-base1.0-dev 


WORKDIR /usr/local/src/opencv_build
RUN cmake \
    -G Ninja \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_LIBRARY_PATH=/usr/local/cuda/lib64/stubs \
    -D BUILD_EXAMPLES=OFF \
    -D BUILD_opencv_python3=ON \
	-D BUILD_opencv_python2=OFF \
    -D BUILD_opencv_java=OFF \
    -D PYTHON_EXECUTABLE='/usr/bin/python3' \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local/ \
    -D CUDA_ARCH_BIN=7.2 \
    -D CUDA_ARCH_PTX= \
    -D CUDA_FAST_MATH=ON \
    -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
    -D ENABLE_NEON=ON \
    -D OPENCV_ENABLE_NONFREE=ON \
    -D OPENCV_EXTRA_MODULES_PATH=/usr/local/src/opencv_contrib/modules \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
    -D WITH_CUBLAS=ON \
    -D WITH_CUDA=ON \
    -D WITH_CUDNN=ON \
    -D OPENCV_DNN_CUDA=ON \
    -D WITH_GSTREAMER=ON \
    -D WITH_LIBV4L=ON \
	-D WITH_NVCUVID=ON \
    /usr/local/src/opencv
RUN ninja -j$(nproc) \
    && ninja install -j$(nproc) \
    && ninja package -j$(nproc)




# TKDNN
WORKDIR /usr/local/src
ARG TTAG=master
RUN git clone --depth 1 --branch ${TTAG} https://github.com/ceccocats/tkDNN.git \
    && mkdir tkdnn_build
WORKDIR /usr/local/src/tkdnn_build
RUN cmake \
    -G Ninja \
    -D CMAKE_INSTALL_PREFIX=/usr/local/tkdnn \
    /usr/local/src/tkDNN
RUN ninja -j$(nproc) \
    && ninja install -j$(nproc)


# FINAL IMAGE
FROM ${BASE_IMAGE}

RUN apt-get update && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install --no-install-recommends \
    libyaml-cpp0.5v5 python3-numpy \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# install opencv
COPY --from=builder /usr/local/src/opencv_build/OpenCV-*-aarch64.sh /tmp/
RUN /tmp/OpenCV-*-aarch64.sh --skip-license --prefix=/usr/local \
    && rm /tmp/OpenCV-*-aarch64.sh

# install tkdnn
# COPY --from=builder /usr/local/tkdnn /usr/local/tkdnn
# RUN echo "/usr/local/tkdnn/lib" > /etc/ld.so.conf.d/tkdnn.conf \
#     && ldconfig
# ENV PATH=$PATH:/usr/local/tkdnn/bin
RUN apt update -y && apt install curl unzip -y
COPY --from=builder /usr/local/tkdnn/bin /usr/local/bin
COPY --from=builder /usr/local/tkdnn/lib /usr/local/lib
COPY --from=builder /usr/local/src/tkDNN /usr/local/src/tkDNN

